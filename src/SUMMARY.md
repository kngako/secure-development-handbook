# Summary

- [Overview](./overview.md)
- [Risk Model](./risk.md)
- [Open Questions](./open-questions.md)
- [Connectivity](./connectivity.md)
- [Tapir](./tapir.md)
    - [Packet Format](./packet_format.md)md
    - [Authentication Protocol](./authentication_protocol.md)
- [Cwtch Library](./cwtch.md)
    - [Groups](./groups.md)
- [Cwtch UI](./ui.md)
    - [Profile Encryption & Storage](./profile_encryption_and_storage.md)
    - [Android Service](./android.md)
    - [Message Overlays](./overlays.md)
    - [Input](./input.md)
- [Cwtch Servers](./server.md)
    - [Key Bundles](./key_bundles.md)
- [Development](./development.md)
- [Deployment](./deployment.md)
- [References](./references.md)


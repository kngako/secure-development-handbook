# Open Research Questions

The only way to build secure software is to attack it from every angle. 
There are a number of limitations and problems with Cwtch, both as designed 
and as implemented in our prototype. We are working on many of these, 
but would like to invite researchers to work with us on new solutions, 
as well as find new problems.

Here are the problems we know about:

### **The User Experience of Metadata Resistance Tools**

Environments that offer
 metadata resistance are plagued with issues that impact usability, e.g. 
 higher latencies than seen with centralized, metadata-driven systems, or dropped connections 
 resulting from unstable anonymization networks. Additional research is needed to understand 
 how users experience these kinds of failures, and how apps should handle and/or communicate them to users.

### **Scalability**

Heavily utilized Cwtch servers increase message latency, and
 the resources a client requires to process messages. While Cwtch servers are 
 designed to be cheap and easy to set up, and Cwtch peers are encouraged to 
 move around, there is a clear balance to be found between increasing the 
 anonymity set of a given Cwtch server (to prevent targeted disruptions) 
 and the decentralization of Cwtch groups.

### **The (Online) First Contact Problem**

Cwtch requires that any two peers are
  online at the same time before a key exchange/group setup is possible. 
  One potential way to overcome this is through encoding an additional public 
  key and a Cwtch server address into a Cwtch peer identifier. This would allow 
  peers to send encrypted messages to an offline Cwtch peer via a known server, 
  with the same guarantees as a Cwtch group message. 
  This approach is not without issues, as by encoding metadata into the Cwtch 
  identifier we allow adversaries to mount partially targeted attacks 
  (in particular denial-of-service attacks against the Cwtch server with the 
  aim of disrupting new connections). However, the benefit of first contact 
  without an online key exchange is likely worth the potential DoS risk in many 
  threat models.
  
Note: Something like [niwl](https://git.openprivacy.ca/openprivacy/niwl) may now allow us to
overcome this problem via fuzzy message detection and offline message retrieval.

### **Reliability**

In Cwtch, servers have full control over the number of messages they store and for how long. This has
 an unfortunate impact on the reliability of group messages: if groups choose an unreliable server, they might find 
 their messages have been dropped. While we provide a mechanism for detecting dropped/missing messages, we do not
  currently provide a way to recover from such failures. There are many possible strategies from asking peers to resend 
  messages to moving to a different server, each one with benefits and drawbacks. A full evaluation of these approaches 
  should be conducted to derive a practical solution.

### **Discoverability** of Servers

Much of the strength of Cwtch rests on the assumption that peers and groups can change
 groups at any time, and that servers are untrusted and discardable. However, in this paper we have not introduced any 
 mechanism for finding new servers to use to host groups.  We believe that such an advertising mechanism could be built 
 ver Cwtch itself.

# Packet Format

All tapir packets are fixed length (8192 bytes) with the first 2 bytes indicated the actual length of the message, 
`len` bytes of data, and the rest zero padded:

    | len (2 bytes) | data (len bytes) | paddding (8190-len bytes)|

Once encrypted, the entire 8192 byte data packet is encrypted using [libsodium secretbox](https://libsodium.gitbook.io/doc/secret-key_cryptography/secretbox) using the standard structure (
note in this case the actual usable size of the data packet is 8190-14 to accommodate the nonce included by secret box)

For information on how th secret key is derived see the [authentication protocol](./authentication_protocol.md)

